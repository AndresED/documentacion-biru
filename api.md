# Servicio Rest - Biru

# Consideraciones

 - El presente proyecto fue desarrollado en [NestJS](http://nestjs.com/)
 - Para la conexión al gestor de base de datos se uso a [Sequelize](https://sequelize.org/) como ORM en su versión para [Typescript](https://www.npmjs.com/package/sequelize-typescript)
 - Se usó como gestor de base de datos MYSQL aunque puede ser cambiado por otro gestor siempre y cuando sea uno relacional.
 - Se usó como servidor de correos  a GMAIL.
 - Se uso el paquet [firebase-admin](https://www.npmjs.com/package/firebase-admin) para conectarnos con firebase y poder realizar la autenticación con redes sociales.
 - Se uso [culqi](https://docs.culqi.com/#/) como pasarela de pago
  - Se uso el paquete [quicktumb](https://www.npmjs.com/package/quickthumb) para la optimización de imagenes. Se recomienda leer la documentación de esta para sacar el máximo provecho de las imágenes almacenadas en el api.
 
 # Instalación de dependencias

- Para el desarrollo de la aplicación se uso yarn como gestor de dependencias pero tambien puede usarse npm.
Para la instalación ejecutar `npm install`   o  `yarn install` 
- Quicktumb require la instalación de la dependencia llamada imagemagick a nivel de sistema operativo. Para instalar ejecutar el siguiente comando
```
Ubuntu
apt-get install imagemagick

Mac OS X
brew install imagemagick

Fedora/CentOS
yum install imagemagick
```

# Configuración
Dividiremos la siguiente sección en los siguientes puntos:

 - Configuración de la base de datos 
 - Configuración de la aplicación en facebook developers
 - Configuración de la cuenta en la consola de Google
 - Configuración de firebase
 - Configuración del servidor de correos 
 - Configuración de Culqi
 - Configuración de las variables de entorno

1. **Configuración de la base de datos**

Como se mencionó en las consideraciones , estamos trabajando con mysql como gestor de base de datos por lo que los scripts que se generaron para la creación de la base de datos están orientados con la sintaxis de este gestor. Si bien gracias al ORM podemos trabajar con diferentes gestores debes tener en consideración la sintaxis de los scripts que usaremos.
Los scripts los encontrarán en el repositorio correspondiente a la base de datos. Ejecutarlos en el siguiente orden:

	 - script.sql 
	 - ubicaciones.sql
	 - inserts.sql
	 
En el script inserts.sql se encuentra la cuenta tipo administrador, la contraseña está cifrada y es 123456. Ten la libertad de modificarla.
En el mismo repositorio se encuentra el diagrama de la BD el cual puede ser abierto con MySQL Workbench.

 2. **Configuración de la aplicación en facebook developers**

Este paso así como el que le sigue son importantes para la parte de la autenticación mediante redes sociales.
Como primer paso nos dirigimos a la pagina de [facebook developers](https://developers.facebook.com/) .En el menú superior elegimos la opción **Mis aplicaciones** 

Nos aparecerá una ventana similar a esta: 
![enter image description here](https://i.imgur.com/gGiMGqU.png)

Presionamos añadir nueva aplicación. Nos saldrá lo siguiente:

![enter image description here](https://i.imgur.com/iyaZEie.png)

Elegimos la opción para todo lo demás y llenamos la información que nos solicita:

![enter image description here](https://i.imgur.com/b8ZGLL3.png)

Una vez creada la aplicación nos aparecerá una ventana similar a esta donde elegimos Inicio de sesión con facebook
![enter image description here](https://i.imgur.com/L3hwCYw.png)

Nos aparecerá para elegir el tipo de plataforma para el cual deseamos implementar el inicio de sesión para ello elegimos web.
![enter image description here](https://i.imgur.com/6xckit7.png)

Elegida la plataforma solo nos queda llenar la información que nos solicita:
![enter image description here](https://i.imgur.com/OMyA0L6.png)

Una vez llenada nos dirigimos a la configuración del proyecto
![enter image description here](https://i.imgur.com/UgGj0G6.png)

Aquí guardamos el  identificador de la aplicación y la clave secreta, ya que la usaremos más adelante para la configuración de firebase.
![enter image description here](https://i.imgur.com/5vfZKfy.png)

 3. **Configuración de la consola de google**

Como primer instancia ingresamos a la consola de [desarrolladores de google](https://console.developers.google.com/?hl=ES).
Estando aquí como primer paso activaremos el api de google que nos permite iniciar sesión mediante este proveedor para ello elegimos ENABLE APIS AND SERVICES

![enter image description here](https://i.imgur.com/5R9exDs.png)
Aquí nos aparecerán los diferentes servicios que google tiene para nosotros pero el que a nosotros nos importa es Google + Api
![enter image description here](https://i.imgur.com/tLqiatp.png)

Ingresamos y activamos el servicio:
![enter image description here](https://i.imgur.com/ftUsEni.png)

Hecho esto podemos regresar a la [consola de desarrolladores](https://console.developers.google.com/?hl=ES) e ingresar a la opción credenciales

![enter image description here](https://i.imgur.com/rbPVZvr.png)

En esta página vamos a proceder a crear las keys que usaremos para la autenticación mediante google.

![enter image description here](https://i.imgur.com/wPTG9je.png)

Las claves que generaremos serán del tipo OAuth
En tipo de aplicación seleccionamos aplicación web
![enter image description here](https://i.imgur.com/v2HTuqC.png)

Colocaremos un nombre para la aplicación y en las opciones autorizadas por el momento las dejas en blanco posteriormente en la configuración de firebase regresaremos a esta parte a agregar unas cosas extra.

![enter image description here](https://i.imgur.com/dHSTSsw.png)

Realizado este paso se nos generarán dos datos importantes que usaremos en la configuración de firebase por lo que guardarlos al igual que lo hicimos con Facebook.

![enter image description here](https://i.imgur.com/gGKGzUT.png) 

 4. **Configuración de Firebase**
Nos dirigimos a la [consola de firebase](https://console.firebase.google.com/) y le damos agregar proyecto
![enter image description here](https://i.imgur.com/3aJmMUr.png)

Seguimos los pasos  que nos indican para crear nuestro proyecto:
![enter image description here](https://i.imgur.com/YCrrpBN.png)


Una vez  creado nos dirigimos a la opción Authentication y seleccionamos la pestaña método de autenticación:
![enter image description here](https://i.imgur.com/JhrKh3U.png)

![enter image description here](https://i.imgur.com/68PByFm.png)

En este punto es necesario tener a mano abierto tanto la [consola de google](https://console.cloud.google.com/?hl=es-419) como la página de [desarrolladores de facebook](https://developers.facebook.com/) .
Primero habilitaremos el inicio de sesión de fb, para ello damos click sobre este y activamos el switch.
![enter image description here](https://i.imgur.com/cNl57YA.png)

Aquí podremos notar 3 datos importantes , los dos primeros son datos que nos solicitan y que podemos encontrar en la página de fb devlopers ingresando en la  página de información básica de una app y que anteriormente mencionamos guardar.

![enter image description here](https://i.imgur.com/5vfZKfy.png)

Bueno aquí pegamos el app id y clave secreta pero al mismo tiempo copiamos el URI de redireccionamiento de OAuth que nos da firebase y dirigimos a la configuración del inicio de sesión y pegamos en el campo URI de redireccionamiento válido el URI anteriormente copiado de firebase.
![enter image description here](https://i.imgur.com/hvIE5aj.png)

Hecho esto le damos guardar y con esto ya tendremos habilitado el inicio de sesión con fb.
Algo similar haremos con google 
![enter image description here](https://i.imgur.com/IPAOzpj.png)
Una ve habilitado procederemos a colocar el nombre de tu proyecto y seleccionaremos el correo de asistencia del proyecto(Te aparecerá una lista de correos permitidos en tu panel de firebase por defecto aparecerá el email del propietario del proyecto) . Llenado esto pasamos a configurar en la misma sección la Configuración de SDK . Cuando vamos a configurar notamo que nos solicita la misma información que no proporciono  google

![enter image description here](https://i.imgur.com/9IbZ9R3.png)

Aqui pegaremos lo que google nos proporciono y le damos guardar. Con esto ya tendremos configurado  firebase para el inicio de sesión mediante fb y google. Pero como pasos finales tendremos que agregar el mismo URI de redireccionamiento de OAuth  que pegamos en la pagina de desarrolladores de fb en el proyecto creado en la consola de google para ello  nos dirigimos a eesta y seleccionamos la key que creamos anteriormente:
![enter image description here](https://i.imgur.com/fKAd6I7.png)

Aqui le daremos cliclk sobreagregar URI en URI de redireccionamiento autorizado y pegamos el mismo qu pegamos en la pagina de desarrolladores de fb:

![enter image description here](https://i.imgur.com/25Y0uFh.png)

Hecho esto le damos guardar y con esto ya tendriamos la configuración realiada aunque no finalizada ya que como paso final debemos descargar la key:
![enter image description here](https://i.imgur.com/zIPCkJJ.png)

Una vez descargado abrimos el archivo , copiamos su contenido y nos redirimos al api que clonamos de este repositorio y editamos el archivo llamado firebase.json y reemplazamos su contenido con el del archivo descargado. Hecho esto podremos decir que tenemos la configuración de firebase finalizado.

5. **Configuración del servidor de correos** 

No hay mucha ciencia en esta sección para la configuración del servidor de correos haremos uso de la misma información que el mismo google nos da  

 - **servidor smtp:**  smtp.gmail.com 
 - **puerto:**  465 
-  **ssl:** true 
 - **usuario:** un correo de gmail 
 - **contraseña:** una contraseña de aplicación generada
   para el correo de gmail.

Lo unico que se debe gestionar aqui es la clave de aplicación pero para ello recomendamos [leer la misma documentación que google nos da para ello.](https://support.google.com/mail/answer/185833?hl=es-419)



**6. Configuración de Culqi**
En esta parte como primera instancia [debemos crear nuestra cuenta en culqi](https://afiliate.culqi.com/online) 
Una vez creada nuestra cuenta [iniciamos sesión](https://integ-panel.culqi.com/#/login) y procedemos a crear nuestro comercio
![enter image description here](https://i.imgur.com/xwUQFWH.png)




![enter image description here](https://i.imgur.com/OXzHbZG.png)

Una vez creado nos dirigimos a  desarrollo-> api keys
![enter image description here](https://i.imgur.com/GCJuHWR.png)

En esta parte podemos obtener dos datos muy importantes que nos serviran para configurar nuestras variables de entorno y son la llave publica y privada. La publica la tenemos marcada en la vista y la privada la obtienes  al presionar en ver.

**7. Configuración de las variables de entorno**
Llegamos a la parte final de las configuraciones. Aqui colocaremos información de la conexion a la base de datos y información que en los pasos anteriores obtuvimos
Como primera instancia creamos un archivo llamado .env en la raiz de la carpeta del api y copiamos el contenido del archivo llamado .env.example  que esta en la raiz y que tiene la siguiente estructura:

```
# STAGE
# VALUES: production, development
STAGE = development

# DATABASE

USERNAME_DB = 
DATABASE = 
PASSWORD_DB = 
HOST_DATABASE = 
DIALECT = 
TYPE_SGBD = 
LOGGIN_DATABASE = 
OPERATORSALIASES = 
PORT_DB = 

# SERVER
PORT = 
PORT_SOCKET = 

# WEBAPP
ROUTE_WEB_APP = 

# JWT
SECRET = SECRET
EXPIRESIN = 1d
DEFAULTSTRATEGY = jwt

# STORAGE
STORAGE = 
LIMIT_UPLOAD = 300mb

# MAIL
MAIL_HOST = 
MAIL_SERVICE = 
MAIL_USER = 
MAIL_PASSWORD = 
MAIL_EMAIL_FR0M = 
MAIL_PORT = 
MAIL_SECURE = 
MAIL_REJECT_UNAUTHORIZED = 

#CULQI
CULQI_PUBLIC_KEY = 
CULQI_PRIVATE_KEY = 

#FIREBASE
FIREBASE_DATABASE_URL = 
`````




  


 Pasaremos a explicar bloque por bloque este archivo y como configurarlo:

Como primer instancia tenemos el modo en que será ejecutada el api si
   bien en el package.json se han creado comandos en especifico para
   poder ejecutar ambos entornos esta variable nos permite a nivel de codigo realizar ciertas validaciones para la busqueda de path de archivos. Los valores que puede tomar son production y development.
`````
# STAGE
# VALUES: production, development
STAGE = development
`````

El presente bloque hace referencia a la conexión a la base de datos , para tener una mayor referencia acerca de estas variables les recomendamo [leer la documentación de sequelize](https://sequelize.org/master/manual/dialect-specific-things.html)

`````
# DATABASE
USERNAME_DB = root
DATABASE = birudb
PASSWORD_DB = 123456
HOST_DATABASE = localhost
DIALECT = mysql
TYPE_SGBD = mysql
LOGGIN_DATABASE = false
OPERATORSALIASES = false
PORT_DB = 3306
`````

El presente bloque hace referencia a los puertos que haran uso e servicio rest y el servidor de sockets. Pueden trabajar bajo el mismo puerto pero no se recomienda.

`````
# SERVER
PORT = 3000
PORT_SOCKET = 5000
`````

Una de las caracteristicas de la aplicación es el envio de emails para la recuperación de la contraseña. Ante esta operación el api envia un url para la recuperación pero para poder generar el enlace correctamente  en este bloque debemos colocar el link de la aplicación ya subida a produccion o en desarrollo.
`````
# WEBAPP
ROUTE_WEB_APP = http://localhost:4200
`````

La aplicación  para su seguridad hace uso de la generación de tokens mediante JWT en el presente bloque solicitamos necesaria para la generación de los tokens como son el secret que viene a ser una cadena alfanumerica que sirve para generar los tokens (se recomienda usar uno seguro que contenga numeros,letras,caracteres especiales,etc). el expiresin qu hace referencia al tieempo de vida del token y el defaultstrategy que en si es el tipo o método que usaremos para generar el token en nuestro caso estamos usando jwt.

`````
# JWT
SECRET = SECRET
EXPIRESIN = 365d
DEFAULTSTRATEGY = jwt
`````

Una de las caracteristicas de la aplicación es que nos permite almacenar archivos y acceder a estos cuando se los requiera , para ello la manera como te los devuelve es como un enlace directo al api para ello se requiere especificar el url del api ya en production o desarrollo para que al momento de hacer una consulta te traiga correctamente el enlace del archivo al cual se quiere acceder.
La variable LIMIT_UPLOAD hace mención al tamaño maximo de subida de un archivo.

`````
# STORAGE
STORAGE = http://localhost:3000/
LIMIT_UPLOAD = 300mb
`````

En esta sección usaremos los datos que nos proporciiona google asi como la contraseña que se genero anteriormente.
`````
# MAIL
MAIL_HOST = smtp.gmail.com
MAIL_SERVICE = gmail
MAIL_USER = @gmail.com
MAIL_PASSWORD = micorreo@gmail.com
MAIL_EMAIL_FR0M = contacto@biru.com.pe
MAIL_PORT = 465
MAIL_SECURE = true
MAIL_REJECT_UNAUTHORIZED = false
`````

Para la configuración de culqi no hay mucha ciencia solo ponemos las keys obtenidas en el dashboard de culqi

`````
#CULQI
CULQI_PUBLIC_KEY = pk_test_CSqJHGZTNUynWSBV
CULQI_PRIVATE_KEY = sk_test_zjHlCacYkWqB5lPH
 `````
Para finalizar necesitamos colocar el url de la base de datos de firebase
 `````
#FIREBASE
FIREBASE_DATABASE_URL = https://biru-7a6cb.firebaseio.com
Servidor de desarrollo y Despliegue
 `````
Para ello nos dirigimo a la consola de firebase , nos vamos a nuestro proyecto y en la opcion  Descripción general y configuración de proyecto:

![enter image description here](https://i.imgur.com/TzGusBL.png)

En la pestaña cuentas de servicio encontraremos el databaseUrl, lo copiamos y pegamos en nuestra variable de entorno

![enter image description here](https://i.imgur.com/UnnefuF.png)

Realizado esto pasos correctamente ya podemos levantar nuestro servidor ya sea en desarrollo o producción

# Levantar el servidor

### Desarrollo  
 `npm run start:dev`  -  Permite levantar el servidor de desarrollo que estará disponible en `http://localhost:3000/`  
### Producción
 `npm run start ` - Permite levantar el servidor de desarrollo que estará disponible en `http://localhost:3000/` 

Deben tener en cuenta la variable de entorno STAGE al momento de ejecutar  los comandos de lo contrario tendran errores.