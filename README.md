# Documentación - Biru
A continuación se presentan la documentación respectiva para el levantamiento y despliegue de la aplicación Biru

 - [Webapp](https://gitlab.com/AndresED/documentacion-biru/-/blob/master/webapp.md)
 - [Dashboard](https://gitlab.com/AndresED/documentacion-biru/-/blob/master/dashboard.md)
 - [Servicio rest](https://gitlab.com/AndresED/documentacion-biru/-/blob/master/api.md)

