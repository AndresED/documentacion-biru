# Panel de administración - Biru

El presente proyecto fue creado usando [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

# Instalación de dependencias

Para el desarrollo de la aplicación se uso yarn como gestor de dependencias pero tambien puede usarse npm.
Para la instalación ejecutar `npm install`   o  `yarn install` 

# Configuraciones adicionales

Todas las configuraciones de la aplicación se realizaran en la carpeta `src/environments`. Cabe recalcar que en esta carpeta encontraremos dos archivos  `environments.ts` y `environments.prod.ts`. Aqui encontraran un objeto en donde configuraran los datos de conexión al api rest:

```
export  const  environment = {
	production:  false,
	secure_socket:  false,
	server_api:  'http://localhost:3000',
	server_socket:  'http://localhost:5000',
	webapp:  'https://biru.wost.pe'
};
`````

**Donde** 

 - `production` hace referencia al estado del servidor , true production - false desarrollo
 - `secure_socket` hace referencia al tipo de conexión con el servidor de sockets , true conexión segura - false conexión insegura(activar cuando esten usando un certificado ssl sino generará error de CORS).
 - `server_api` - url del api rest 
 - `server_socket` - url del servidor de sockets
 - `webapp` - url de la aplicación web
  
# Servidor de desarrollo

  
Ejecute `ng serve` para levantar un servidor de desarrollo. Puede visualiar en `http://localhost:4200/`. La aplicación se recargará automáticamente si cambia alguno de los archivos de origen.


# Despliegue a producción

Ejecute `ng build` para generar los archivos para el despliegue de la aplicación , estos los encontrarás en la carpeta `dist/`. Puedes usar la bandera `--prod` para construir los archivos finales para producción.

