# Aplicación web - Biru

# Características

 - El presente proyecto fue creado usando [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.
 - El presente proyecto implementa el proceso denominado **SSR (server side rendering)**.  La herramienta que usaremos para aplicar SSR en nuestro proyecto es [**Angular Universal**](https://angular.io/guide/universal).
# ¿Angular Universal?

Es una  **tecnología que nos permitirá ejecutar nuestra aplicación Angular desde el servidor**. Genera páginas de aplicaciones estáticas en el servidor mediante el proceso denominado  **SSR (server side rendering) (renderizado de lado del servidor).**

Básicamente nos servirá para cubrir las áreas oscuras que se dan en el desarrollo moderno:

-   Facilitar la indexacion del SEO
-   Mejorar el rendimiento de la aplicacion en dispositivos de baja potencia
-   Mostrar la primera pagina rápidamente


# Instalación de dependencias

Para el desarrollo de la aplicación se uso yarn como gestor de dependencias pero tambien puede usarse npm.
Para la instalación ejecutar `npm install`   o  `yarn install` 




# Configuraciones adicionales

Todas las configuraciones de la aplicación se realizarán en la carpeta `src/environments`. Cabe recalcar que en esta carpeta encontraremos dos archivos  `environments.ts` y `environments.prod.ts`. Aquí encontraran un objeto en donde configuraran los datos de conexión al api rest:

```
export  const  environment = {
	production:  false,
	secure_socket:  false,
	server_api:  '',
	server_socket:  '',
	googleMapKey:  '',
	firebaseConfig: {
		apiKey:  "",
		authDomain:  "",
		databaseURL:  "",
		projectId:  "",
		storageBucket:  "",
		messagingSenderId:  "",
		appId:  ""
	},
	webapp:  ''
};;
`````

**Donde** 

 - `production` hace referencia al estado del servidor , true production - false desarrollo
 - `secure_socket` hace referencia al tipo de conexión con el servidor de sockets , true conexión segura - false conexión insegura(activar cuando esten usando un certificado ssl sino generará error de CORS).
 - `server_api` - url del api rest 
 - `server_socket` - url del servidor de sockets
 - `webapp` - url de la aplicación web
 - `googleMapKey` - key proporcionada por google para el renderizado del mapa en la pagina nosotros.
 - `firebaseConfig` - Información obtenida en la consola de firebase
  
# Configuración de firebase
  
  
 - Dirigirse a la consola  de firebase e ingresar al proyecto creado para biru:
 ![enter image description here](https://i.imgur.com/3aJmMUr.png)

  

 - Click en descripción del proyecto e ingresar a las configuraciones del proyecto
![enter image description here](https://i.imgur.com/TzGusBL.png)
 
 - En la pestaña  General desplazarse hacia abajo y copiar el contenido de la variable firebaseConfig.
 
![enter image description here](https://i.imgur.com/coZkg9n.png)


![enter image description here](https://i.imgur.com/jt5GKSk.png)

 - Pegar el contenido de esta variable en el archivo `environments.ts` y `environments.prod.ts`

# Servidor de desarrollo y Despliegue

# Como una aplicación tradicional
Pese al haberse configurado con Angular SSR también podemos levantar el servidor de desarrollo y generar el compilado como si fuera una aplicación de angular tradicional para ello  podemos hacer uso de los siguientes comandos

 - `ng serve` - Permite levantar el servidor de desarrollo que estará disponible en `http://localhost:4200/`  
 - `ng build` - Permite generar una compilación del proyecto el cual puede ser subido a producción aunque no se recomienda usar esta versión para proyectos con usuario finales.
 - `ng build --prod` - Permite generar una compilación del proyecto el cual puede ser subido a producción.
# Como una aplicación SSR

### Desarrollo  
 1. `npm run dev:ssr`  -  Permite levantar el servidor de desarrollo que estará disponible en `http://localhost:4200/`  
### Producción
El proceso de despliegue de una aplicación SSR consta de dos partes. La primera que nos permite generar los compilados del proyecto y la segunda crear un servidor el cual levantará nuestra aplicación SSR haciendo uso de nodejs.
 1. `npm run build:ssr ` - Genera el compilado
 2. ` npm run start`   - Después de compilarlo para iniciar el servidor
